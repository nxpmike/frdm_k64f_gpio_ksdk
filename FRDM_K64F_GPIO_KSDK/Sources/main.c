/*
 * [File Name]     main.c
 * [Platform]      Freescale FRDM-KXXF Evaluation Board
 * [Project]       Project Description
 * [Version]       1.00
 * [Author]        Mike Steffen
 * [Date]          Sept 30, 2014
 * [Language]      'C'
 * [History]       1.00 - Original Release (Uploaded to GIT)
 *
 * Copyright (c) 2014, Freescale, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


//-----------------------------------------------------------------------
// Standard C/C++ Includes
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------
// KSDK Includes
//-----------------------------------------------------------------------
#include "fsl_device_registers.h"                                    // Header file for generic MCU definitions
#include "fsl_gpio_hal.h"                                            // Header file for GPIO HAL registers
#include "fsl_sim_hal.h"                                             // Header file for clock gate HAL registers
#include "fsl_port_hal.h"                                            // Header file for GPIO mux HAL registers
#include "fsl_pit_hal.h"                                             // Header file for PIT HAL functions

//-----------------------------------------------------------------------
// Application Includes
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------
// Constants
//-----------------------------------------------------------------------
#define GPIOE_INSTANCE    4                                          // Instance for PORT module: PORTA=0, PORTB=1, PORTC=2, etc...
#define PIT_INSTANCE      0                                          // Instance for PIT module PIT0=0, PIT1=1, etc...
#define PORT_PIN          0x1A                                       // PORT pin 26
#define PIT_MODULE        0                                          // PIT Module number 0,1,2,3,etc...

//-----------------------------------------------------------------------
// Typedefs
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------
// Global Variables
//-----------------------------------------------------------------------
uint32_t pitFreq;                                                    // System clock / 2
uint32_t pitCount;                                                   // Starting count value for PIT timer
uint32_t us;                                                         // number of us needed

//-----------------------------------------------------------------------
// Macros
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------
// Function Prototypes
//-----------------------------------------------------------------------
extern void PIT0_IRQHandler(void);

//-----------------------------------------------------------------------
// Main Function
//-----------------------------------------------------------------------
int main(void)
{

    // Set up Clock Gates, Pin Mux, and GPIO
    SIM_HAL_EnablePortClock(SIM_BASE, GPIOE_INSTANCE);                   // Enable Port E Clock Gate
    SIM_HAL_EnablePitClock(SIM_BASE, PIT_INSTANCE);                      // Enable PIT Clock Gate
    PORT_HAL_SetMuxMode(PORTE_BASE, PORT_PIN, kPortMuxAsGpio);           // Configure PORTE, Pin 26, MUX as a GPIO
    GPIO_HAL_SetPinDir(PTE_BASE, PORT_PIN, kGpioDigitalOutput);          // Configure PORTE, Pin 26, as an Output

    //Enable PIT interrupt in the NVIC Vector table
    NVIC_EnableIRQ(PIT0_IRQn);                                           // Enable PIT0 IRQs in the NVIC

    // Initialize PIT 0 Timer
    us = 100000;                                                         // Set the number of micro seconds needed
    pitFreq = (DEFAULT_SYSTEM_CLOCK / 2);                                // PIT runs off bus clock, set to 50% of system clock
    pitCount  = (uint32_t)((us * (pitFreq / 1000000U)) - 1U);            // Calculate the count value for the PIT timer
    PIT_HAL_Enable(PIT_BASE);                                            // Enable PIT0 Timer
    PIT_HAL_SetTimerRunInDebugCmd(PIT_BASE, true);                       // Allow PIT0 timer to run in Debug Mode
    PIT_HAL_StopTimer(PIT_BASE, 0);                                      // Stop PIT0 Timer (to make changes to the PIT Timer)
    PIT_HAL_SetTimerPeriodByCount(PIT_BASE, 0, pitCount);                // PIT0 Timer count value
    PIT_HAL_SetIntCmd(PIT_BASE, 0, true);                                // Enable PIT0 interrupt

    if(PIT_HAL_IsIntPending(PIT_BASE, 0))                                // Check for pending PIT interrupt
    {
        PIT_HAL_ClearIntFlag(PIT_BASE, 0);                               // Clear PIT interrupt flag
    }

    PIT_HAL_StartTimer(PIT_BASE, 0);                                     // Start PIT0 Timer

    for (;;)                                                             // forever loop
    {

    }

    return 0;                                                            // Main.c return

}

//-----------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------

/*!
 *
 * @brief Interrupt handler for PIT0 interrupts
 *
 * @param none
 * @return none
 *
 */

void PIT0_IRQHandler(void)
{
    PIT_HAL_ClearIntFlag(PIT_BASE, 0);               // Clear PIT interrupt flag
    GPIO_HAL_TogglePinOutput(PTE_BASE, PORT_PIN);    // Toggle GREEN LED
}

////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////

